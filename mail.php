<meta charset="utf-8">
<?php
	$gender = mb_convert_encoding(htmlspecialchars($_POST['gender']), "SJIS", "UTF-8");
	$field_name = mb_convert_encoding(htmlspecialchars($_POST['name']), "SJIS", "UTF-8");
	$field_email = mb_convert_encoding(htmlspecialchars($_POST['email']), "SJIS", "UTF-8");
	$field_message = mb_convert_encoding(htmlspecialchars($_POST['message']), "SJIS", "UTF-8");
	$field_career = mb_convert_encoding(htmlspecialchars($_POST['career']), "SJIS", "UTF-8");
	$field_graduation = mb_convert_encoding(htmlspecialchars($_POST['graduation']), "SJIS", "UTF-8");
	$mail_to = 'recruit@oceansoftware.co.jp';
	$subject = mb_convert_encoding('採用サイトから応募がありました ', "SJIS", "UTF-8").$field_name;
	if ($gender == 1) {
		$body_message = mb_convert_encoding('新卒採用エントリー', "SJIS", "UTF-8")."\n";
	} else {
		$body_message = mb_convert_encoding('中途採用エントリー', "SJIS", "UTF-8")."\n";
	}
	$body_message .= 'From: '.$field_name."\n";
	$body_message .= 'E-mail: '.$field_email."\n";
	$body_message .= mb_convert_encoding('卒業学校: ', "SJIS", "UTF-8").$field_career."\n";
	$body_message .= mb_convert_encoding('卒業年月: ', "SJIS", "UTF-8").$field_graduation."\n";
	$body_message .= 'Message: '.$field_message;
	$headers = 'From: '.$field_email."\r\n";
	$headers .= 'Reply-To: '.$field_email."\r\n";

	if ($field_name) {
		$mail_status = mail($mail_to, $subject, $body_message, $headers);
		if ($mail_status) {
?>
    <script type="text/JavaScript">
    	alert('メッセージを送信しました。');
        window.location = '/';
    </script>
<?php
}
		else { ?>
    <script type="text/JavaScript">
        alert('メッセージ送信に失敗しました。こちらのメールアドレスへお問い合わせください。info@oceansoftware.co.jp');
        window.location = '/';
    </script>
<?php
		}
	}
?>
